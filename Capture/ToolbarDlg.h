#pragma once


// CToolbarDlg 对话框

class CToolbarDlg : public CDialog
{
	DECLARE_DYNAMIC(CToolbarDlg)

public:
	CToolBar		m_toolbar;
	CImageList		m_imagelist;

	CToolbarDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CToolbarDlg();

// 对话框数据
	enum { IDD = IDD_TOOLBAR_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
	virtual void OnCancel();
public:
	afx_msg void OnClose();
	afx_msg void OnBnClickedToolBar( UINT nID );
};

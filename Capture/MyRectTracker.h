#pragma once


// CMyRectTracker

class CMyRectTracker : public CRectTracker
{
public:
	void Draw(CDC* pDC) const;
	CMyRectTracker();
	virtual ~CMyRectTracker();

	void DrawTrackerRect(LPCRECT lpRect, CWnd* pWndClipTo, CDC* pDC, CWnd* pWnd);
};
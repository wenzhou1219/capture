#pragma once
#include "afxwin.h"

class CScreenBitmap :
	public CBitmap
{
public:
	CScreenBitmap(void);
	virtual ~CScreenBitmap(void);
	void SetSize(int cx, int cy);
	void GetScreenBmp(CDC *pScreenDC);
	void BitBltTo(CDC *pDC, RECT *pRect=NULL);
	void CopyToClipboard(HWND hwnd, RECT *pRect=NULL);
	HBITMAP CopyBitmap(RECT *pRect=NULL);
	BOOL ExportToFile(CDC *pDC, TCHAR *szBmpName,  RECT *pRect=NULL);

public:
	int m_nCxScreen;
	int m_nCyScreen;
	BITMAPINFO *m_pbmi;
	BYTE *m_pBits;
};

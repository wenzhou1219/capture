//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Capture.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CAPTURE_DIALOG              102
#define IDD_CAPTURE_DIALOG1             103
#define IDD_COVER_DIALOG                103
#define IDD_TOOLBAR_DIALOG              104
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     129
#define IDB_BITMAPTEST                  129
#define IDB_SAVE                        130
#define IDB_CANCEL                      131
#define IDB_OK                          132
#define IDI_CANCEL                      133
#define IDI_OK                          134
#define IDI_SAVE                        135
#define IDR_TOOLBAR1                    136
#define ID_BUTTON32771                  32771
#define ID_BUTTON32772                  32772
#define ID_BUTTON32773                  32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif


// Capture.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include "jwhotkey.h"

class CCaptureApp : public CWinAppEx
{
public:
	CCaptureApp();
	virtual ~CCaptureApp();

	JWHotkey		m_hotkey;

// 重写
	public:
	virtual BOOL InitInstance();

// 实现

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};

extern CCaptureApp theApp;
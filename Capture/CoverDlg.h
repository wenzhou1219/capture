#pragma once

#include "ToolbarDlg.h"
#include "MyRectTracker.h"

// CCoverDlg 对话框

class CCoverDlg : public CDialog
{
	DECLARE_DYNAMIC(CCoverDlg)

public:

	CCoverDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCoverDlg();

// 对话框数据
	enum { IDD = IDD_COVER_DIALOG };

	INT				m_cxScreen;
	INT				m_cyScreen;
	CRgn			m_wndRgn;
	CRgn			m_screenRgn;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnNcPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnPaint();
	afx_msg void OnClose();
protected:
	virtual void OnOK();
	virtual void OnCancel();
};

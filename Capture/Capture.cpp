
// Capture.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "Capture.h"
#include "CaptureDlg.h"
#include "CoverDlg.h"
#include "ToolbarDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCaptureApp

BEGIN_MESSAGE_MAP(CCaptureApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CCaptureApp 构造

CCaptureApp::CCaptureApp()
{
	m_pMainWnd = NULL;
}

CCaptureApp::~CCaptureApp()
{
	if (NULL != m_pMainWnd)
	{
		delete m_pMainWnd;
	}
}


// 唯一的一个 CCaptureApp 对象
CCaptureApp theApp;

// CCaptureApp 初始化

BOOL CCaptureApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);

	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	AfxEnableControlContainer();

	//采用非模态对话框便于程序一启动就隐藏对话框
	m_pMainWnd = new CCaptureDlg;
	((CCaptureDlg *)m_pMainWnd)->Create(IDD_CAPTURE_DIALOG, NULL);

	//热键绑定当前窗口
	m_hotkey.BindWindow(m_pMainWnd->GetSafeHwnd());
	m_hotkey.SetHotkey(TEXT("Jimwen-Capture"));

	return TRUE;
}

/************************************************************************/
/* 拦截和重导向部分消息                                                        */
/************************************************************************/
BOOL CCaptureApp::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	CCaptureDlg *pMainDlg = (CCaptureDlg *)(theApp.m_pMainWnd);
	CCoverDlg *pCoverDlg = ((CCaptureDlg *)(theApp.m_pMainWnd))->m_pCoverDlg;

	//遮盖层消息传给主窗口响应
	if (pCoverDlg->GetSafeHwnd() == pMsg->hwnd)
	{
		switch (pMsg->message)
		{
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MOUSEMOVE:
		case WM_RBUTTONDOWN:
			pMainDlg->SendMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
			return TRUE;
		}
	}

	//上下左右键调整选择框
	int xOffset = 0, yOffset=0;
	if ((pMainDlg->GetSafeHwnd() == pMsg->hwnd ||
		pCoverDlg->GetSafeHwnd() == pMsg->hwnd)&& 
		pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_LEFT:
		case VK_RIGHT:
		case VK_UP:
		case VK_DOWN:
			//设置tracker的新位置
			xOffset = VK_LEFT == pMsg->wParam ? -1 : 
					  VK_RIGHT == pMsg->wParam ? 1 : 
					  0;
			yOffset = VK_UP == pMsg->wParam ? -1 : 
					  VK_DOWN == pMsg->wParam ? 1 :
					  0;
			pMainDlg->m_rectTracker.m_rect.OffsetRect(xOffset, yOffset);

			//重绘背景和tracker
			pMainDlg->Invalidate(TRUE);

			//重绘工具栏位置
			pMainDlg->ReShowToolbar();
			return TRUE;
		}
	}

	return CWinAppEx::PreTranslateMessage(pMsg);
}

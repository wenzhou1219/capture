#ifndef JWHOTKEY_H_H_H
#define JWHOTKEY_H_H_H

class JWHotkey
{
public:
	JWHotkey();
	JWHotkey(HWND hwnd, UINT nModifier, UINT nKey);
	virtual ~JWHotkey();
	void BindWindow(HWND hwnd);
	INT	 GetHotkeyId();
	BOOL SetHotkey(PTSTR szAtomName);
	BOOL SetHotkey(PTSTR szAtomName, UINT nModifier, UINT nKey);
	BOOL CancelHotkey();
	void GetHotkey(HWND hHotkey);
	void ShowHotkey(HWND hHotkey);

private:
	HWND m_hwnd;
	INT	 m_hotkeyId;
	UINT m_nModifier;
	UINT m_nKey;
	BOOL m_bIsSet;
};

#endif
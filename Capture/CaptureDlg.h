
// CaptureDlg.h : 头文件
//

#pragma once

#include "CoverDlg.h"
#include "MyRectTracker.h"
#include "ScreenBitmap.h"

// CCaptureDlg 对话框
class CCaptureDlg : public CDialog
{
// 构造
public:
	void ReShowToolbar();
	CCaptureDlg(CWnd* pParent = NULL);	 // 标准构造函数
	virtual ~CCaptureDlg();

// 对话框数据
	CMyRectTracker	m_rectTracker;
	CScreenBitmap	m_bmpScreen;
	INT				m_cxScreen;
	INT				m_cyScreen;
	
	BOOL			m_bLeftDown;

	CCoverDlg		*m_pCoverDlg;
	CToolbarDlg		*m_pToolbarDlg;

	enum { IDD = IDD_CAPTURE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2);
	afx_msg void OnNcPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnClose();
	afx_msg void OnBnClickedToolBar(UINT nID);
	
};

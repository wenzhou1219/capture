#include "StdAfx.h"
#include "ScreenBitmap.h"

CScreenBitmap::CScreenBitmap(void)
{
	m_pbmi = NULL;
	m_pBits = NULL;
}

CScreenBitmap::~CScreenBitmap(void)
{
	//删除已经分配的存储区
	if (NULL != m_pbmi)
	{
		delete m_pbmi;
		m_pbmi = NULL;
	}

	if (NULL != m_pBits)
	{
		delete m_pBits;
		m_pBits = NULL;
	}
}

/**
 *功能:	设置屏幕位图的高宽
 *参数:	cx-宽，cy-高，单位为像素
 *返回:	无
 *其他:	2014/05/17 By Jim Wen Ver1.0
**/
void CScreenBitmap::SetSize(int cx, int cy)
{
	m_nCxScreen = cx;
	m_nCyScreen = cy;
}

/**
 *功能:	或的当前屏幕的DDB
 *参数:	pScreenDC-屏幕DC
 *返回:	无
 *其他:	2014/05/17 By Jim Wen Ver1.0
**/
void CScreenBitmap::GetScreenBmp(CDC *pScreenDC)
{
	CDC memDC;

	memDC.CreateCompatibleDC(pScreenDC);

	if (NULL != this->GetSafeHandle())
	{
		this->DeleteObject();
	}
	this->CreateCompatibleBitmap(pScreenDC, m_nCxScreen, m_nCyScreen);


	memDC.SelectObject(this);
	memDC.BitBlt(0, 0, 
				m_nCxScreen, m_nCyScreen,
				pScreenDC,
				0, 0,
				SRCCOPY);

	memDC.DeleteDC();
}

/**
 *功能:	截取当前屏幕位图的指定部分显示到指定DC上，在显示的DC上以0,0开始显示
 *参数:	pDC		-要在其上显示的DC
 *		pRect	-要截取的显示区域,为NULL时指全屏
 *返回:	无
 *其他:	2014/05/17 By Jim Wen Ver1.0
**/
void CScreenBitmap::BitBltTo(CDC *pDC, RECT *pRect/*=NULL*/)
{
	CDC memDC;

	memDC.CreateCompatibleDC(pDC);
	memDC.SelectObject(this);

	if (NULL == pRect)
	{
		pDC->BitBlt(0, 0, 
					m_nCxScreen, 
					m_nCyScreen,
					&memDC, 
					0, 0,
					SRCCOPY);
	}
	else
	{
		pDC->BitBlt(0, 0, 
					pRect->right - pRect->left, 
					pRect->bottom - pRect->top,
					&memDC, 
					pRect->left, pRect->top,
					SRCCOPY);
	}
}

/**
 *功能:	当前屏幕位图的指定部分
 *参数:	pRect	-要复制的部分,为NULL时指全屏
 *返回:	DDB位图句柄
 *其他:	2014/05/17 By Jim Wen Ver1.0
**/
HBITMAP CScreenBitmap::CopyBitmap(RECT *pRect/*=NULL*/ )
{
	CDC memDC;
	HBITMAP hBitmap;
	BITMAP bitmap;

	memDC.CreateCompatibleDC(NULL);

	this->GetObject(sizeof(BITMAP), &bitmap);
	if (NULL != pRect)
	{
		bitmap.bmWidth = pRect->right - pRect->left;
		bitmap.bmHeight = pRect->bottom - pRect->top;
		bitmap.bmWidthBytes = ((bitmap.bmPlanes*bitmap.bmBitsPixel*bitmap.bmWidth + 15) & ~15) >> 3;
	}
	hBitmap = ::CreateBitmapIndirect(&bitmap);

	memDC.SelectObject(hBitmap);

	BitBltTo(&memDC, pRect);

	memDC.DeleteDC();

	return hBitmap;
}

/**
 *功能:	当前屏幕位图的指定部分复制到剪切板上
 *参数:	hwnd	-剪切板所属窗口句柄
 *		pRect	-要复制的部分,为NULL时指全屏
 *返回:	无
 *其他:	2014/05/17 By Jim Wen Ver1.0
**/
void CScreenBitmap::CopyToClipboard(HWND hwnd, RECT *pRect/*=NULL*/ )
{
	HBITMAP hBitmapClip;

	hBitmapClip = CopyBitmap(pRect);
	OpenClipboard(hwnd);
	EmptyClipboard();							//这里会清除之前托管的位图的内存
	SetClipboardData(CF_BITMAP, hBitmapClip);	//注意这里的参数和复制DIB时是不一样的
	CloseClipboard();
}

BOOL CScreenBitmap::ExportToFile(CDC *pDC, TCHAR *szBmpName,  RECT *pRect/*=NULL*/)
{
	HBITMAP hExportBmp;
	BITMAPFILEHEADER bmfh={0};

	/************************************************************************/
	/* 获得数据                                                                     */
	/************************************************************************/

	//获得要导出的DDB位图
	hExportBmp = CopyBitmap(pRect);

	//获得DDB信息
	BITMAPINFOHEADER bmih={0};
	bmih.biSize = sizeof(BITMAPINFOHEADER);
	bmih.biBitCount = 0;
	if (0 == GetDIBits(pDC->GetSafeHdc(), 
					   hExportBmp, 
					   0, 0, 
					   NULL, (BITMAPINFO *)&bmih, 
					   DIB_RGB_COLORS))
	{
		return FALSE;
	}
	
	//删除已经分配的存储区
	if (NULL != m_pbmi)
	{
		free(m_pbmi);
		m_pbmi = NULL;
	}

	if (NULL != m_pBits)
	{
		free(m_pBits);
		m_pBits = NULL;
	}

	//分配新的存储区
	DWORD dwInfoSize;
	DWORD dwBitsSize;

	if (bmih.biBitCount>=16 && BI_BITFIELDS==bmih.biCompression)
	{
		dwInfoSize = sizeof(BITMAPINFOHEADER) + 4*4;//字节对齐，RGBQUAD每个成员存储的内存大小为4字节

		TRACE("%d %d %d\n", sizeof(BITMAPINFO), sizeof(BITMAPINFOHEADER), sizeof(RGBQUAD));
	}
	else if (bmih.biBitCount>=16)
	{
		dwInfoSize = sizeof(BITMAPINFOHEADER);
	}
	else
	{
		dwInfoSize = sizeof(BITMAPINFOHEADER) + 4*4*bmih.biClrUsed;//字节对齐，RGBQUAD每个成员存储的内存大小为4字节
	}
	dwBitsSize = bmih.biHeight * 
				 (((bmih.biBitCount*bmih.biPlanes*bmih.biWidth + 31) & ~31) >> 3);

	//分配对应的内存区
	m_pbmi = (PBITMAPINFO)malloc(dwInfoSize);
	if (NULL == m_pbmi)
	{
		return FALSE;
	}

	m_pBits = (PBYTE)malloc(dwBitsSize);
	if (NULL == m_pBits)
	{
		free(m_pbmi);
		return FALSE;
	}

	//读入位图信息和位图数据
	CopyMemory(m_pbmi, &bmih, sizeof(BITMAPINFOHEADER));
	if (0 == GetDIBits( pDC->GetSafeHdc(), 
						hExportBmp, 
						0, bmih.biHeight, 
						m_pBits, m_pbmi, 
						DIB_RGB_COLORS))
	{
		free(m_pBits);
		free(m_pbmi);
		return FALSE;
	}

	/************************************************************************/
	/* 保存数据                                                                     */
	/************************************************************************/

	//计算文件头信息
	bmfh.bfType = *(WORD *)"BM";
	bmfh.bfSize = sizeof(BITMAPFILEHEADER) + dwInfoSize + dwBitsSize;
	bmfh.bfOffBits = sizeof(BITMAPFILEHEADER) + dwInfoSize;

	//写入到对应BMP文件中
	HANDLE hFile = CreateFile(	szBmpName,
								GENERIC_WRITE, 0,
								NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	CFile m_saveFile(hFile);

	m_saveFile.Write(&bmfh, sizeof(BITMAPFILEHEADER));
	m_saveFile.Write(m_pbmi, dwInfoSize);
	m_saveFile.Write(m_pBits, dwBitsSize);

	m_saveFile.Close();

	::DeleteObject(hExportBmp);

	return TRUE;
}

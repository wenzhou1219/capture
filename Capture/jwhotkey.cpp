#include<stdafx.h>
#include <commctrl.h>
#include <imm.h>

#include "jwhotkey.h"

JWHotkey::JWHotkey(HWND hwnd=NULL, UINT nModifier=MOD_ALT, UINT nKey=VK_SPACE )
{
	m_hwnd		= hwnd;
	m_nModifier = nModifier;
	m_nKey		= nKey;

	m_bIsSet	= FALSE;
}

JWHotkey::JWHotkey()
{
	m_hwnd		= NULL;
	m_nModifier = MOD_ALT;
	m_nKey		= VK_SPACE;

	m_bIsSet	= FALSE;
}

JWHotkey::~JWHotkey()
{
	if (TRUE == m_bIsSet)
	{
		CancelHotkey();
	}
}

/**
 *功能:	当前热键绑定窗口
 *参数:	无
 *返回:	无
 *其他:	2014/05/13 By Jim Wen Ver1.0
**/
void JWHotkey::BindWindow( HWND hwnd )
{
	m_hwnd = hwnd;
}

/**
 *功能:	设置热键
 *参数:	szAtomName--用作热键唯一标识的字符串
 *返回:	设置成功--TRUE，否则FALSE
 *其他:	2014/05/13 By Jim Wen Ver1.0
**/
BOOL JWHotkey::SetHotkey( PTSTR szAtomName )
{
	m_hotkeyId = GlobalAddAtom(szAtomName);

	return m_bIsSet = RegisterHotKey(m_hwnd, m_hotkeyId, m_nModifier, m_nKey);
}

/**
 *功能:	设置热键并自动取消前一次热键设置
 *参数:	szAtomName	--用作热键唯一标识的字符串
 *		nModifier	--热键转换组合
 *		nKey		--热键键值
 *返回:	设置成功--TRUE，否则FALSE
 *其他:	2014/05/13 By Jim Wen Ver1.0
**/
BOOL JWHotkey::SetHotkey( PTSTR szAtomName, UINT nModifier, UINT nKey )
{
	CancelHotkey();

	m_hotkeyId	= GlobalAddAtom(szAtomName);
	m_nModifier	= nModifier;
	m_nKey		= nKey;

	return m_bIsSet = RegisterHotKey(m_hwnd, m_hotkeyId, m_nModifier, m_nKey);
}

/**
 *功能:	取消设置热键
 *参数:	无
 *返回:	无
 *其他:	2014/05/13 By Jim Wen Ver1.0
**/
BOOL JWHotkey::CancelHotkey()
{
	 m_bIsSet = !UnregisterHotKey(m_hwnd, m_hotkeyId);
	 return !m_bIsSet;
}

/**
 *功能:	从指定热键控件获得转换组合和热键键值数据
 *参数:	hHotkey -- 热键控件句柄
 *返回:	无
 *其他:	2014/05/13 By Jim Wen Ver1.0
**/
void JWHotkey::GetHotkey( HWND hHotkey )
{
	WORD wHotkey;
	UINT nTempModifier;

	wHotkey = (WORD) SendMessage(hHotkey, HKM_GETHOTKEY, 0, 0);

	//分拆出热键
	m_nKey = LOBYTE(wHotkey);
	nTempModifier = HIBYTE(wHotkey);

	m_nModifier=0;
	if(nTempModifier & HOTKEYF_ALT)
	{
		m_nModifier |= MOD_ALT;
	}
	if(nTempModifier & HOTKEYF_CONTROL)
	{
		m_nModifier |= MOD_CONTROL;
	}
	if(nTempModifier & HOTKEYF_SHIFT)
	{
		m_nModifier |= MOD_SHIFT;
	}
}

/**
 *功能:	根据当前转换组合和热键键值数据设置指定热键控件显示
 *参数:	hHotkey -- 热键控件句柄
 *返回:	无
 *其他:	2014/05/13 By Jim Wen Ver1.0
**/
void JWHotkey::ShowHotkey( HWND hHotkey )
{
	UINT nTempModifier;

	nTempModifier = 0;
	if(m_nModifier & MOD_ALT)
	{
		nTempModifier |= HOTKEYF_ALT;
	}
	if(m_nModifier & MOD_CONTROL)
	{
		nTempModifier |= HOTKEYF_CONTROL;
	}
	if(m_nModifier & MOD_SHIFT)
	{
		nTempModifier |= HOTKEYF_SHIFT;
	}

	//设置热键
	SendMessage(hHotkey,
				HKM_SETHOTKEY ,
				MAKEWPARAM(MAKEWORD((BYTE)m_nKey, (BYTE)nTempModifier) ,0),
				0);
}

/**
 *功能:	获得当前热键ID
 *参数:	无
 *返回:	无
 *其他:	2014/05/13 By Jim Wen Ver1.0
**/
INT JWHotkey::GetHotkeyId()
{
	return m_hotkeyId;
}

// ToolbarDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Capture.h"
#include "ToolbarDlg.h"
#include "CaptureDlg.h"


// CToolbarDlg 对话框

IMPLEMENT_DYNAMIC(CToolbarDlg, CDialog)

CToolbarDlg::CToolbarDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CToolbarDlg::IDD, pParent)
{

}

CToolbarDlg::~CToolbarDlg()
{
}

void CToolbarDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CToolbarDlg, CDialog)
	ON_WM_CLOSE()
	ON_COMMAND_RANGE(IDB_SAVE, IDB_OK, OnBnClickedToolBar)
END_MESSAGE_MAP()


/************************************************************************/
/* 创建工具栏                                                                  */
/************************************************************************/
BOOL CToolbarDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	::SetWindowLong(this->m_hWnd, GWL_STYLE, WS_POPUP);

	//创建工具栏容器
	if (!m_toolbar.CreateEx(this,
		TBSTYLE_FLAT,
		WS_CHILD | WS_VISIBLE |
		CBRS_TOP |
		CBRS_TOOLTIPS | CBRS_FLYBY |
		CBRS_SIZE_DYNAMIC))
	{
		return -1;
	}

	//添加图片
	m_imagelist.Create(16, 16, ILC_COLOR32 | ILC_MASK, 0, 1);
	m_imagelist.Add(AfxGetApp()->LoadIcon(IDI_SAVE));
	m_imagelist.Add(AfxGetApp()->LoadIcon(IDI_CANCEL));
	m_imagelist.Add(AfxGetApp()->LoadIcon(IDI_OK));

	//添加按钮
	UINT arry[4];
	arry[0] = IDB_SAVE;          
	arry[1]	= ID_SEPARATOR;
	arry[2] = IDB_CANCEL; 
	arry[3] = IDB_OK; 

	m_toolbar.SetButtons(arry, 4);

	//图片对应按钮
	m_toolbar.GetToolBarCtrl().SetImageList(&m_imagelist);
	m_toolbar.SetSizes(CSize(30,30),CSize(16,16));//设置按钮和图片大小
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);

	//设置工具栏窗口属性
	SetWindowPos(((CCaptureDlg *)theApp.m_pMainWnd)->m_pCoverDlg, 
					0, 
					0, 
					100,
					34,
					SWP_HIDEWINDOW | SWP_NOMOVE);

	return TRUE;
}

/************************************************************************/
/* 显示、隐藏和退出全部映射到主窗口上                                          */
/************************************************************************/
void CToolbarDlg::OnOK()
{
	((CCaptureDlg *)AfxGetMainWnd())->OnOK();
}

void CToolbarDlg::OnCancel()
{
	((CCaptureDlg *)AfxGetMainWnd())->OnCancel();
}

void CToolbarDlg::OnClose()
{
	((CCaptureDlg *)AfxGetMainWnd())->OnClose();
}

/************************************************************************/
/* 工具栏响应                                                                   */
/************************************************************************/
void CToolbarDlg::OnBnClickedToolBar( UINT nID )
{
	//将焦点返回到遮盖层窗口以便进行微调操作
	((CCaptureDlg *)AfxGetMainWnd())->m_pCoverDlg->SetFocus();

	//不同按钮时的响应
	switch (nID)
	{
	case IDB_OK:
		//复制到剪切板上
		((CCaptureDlg *)AfxGetMainWnd())->m_bmpScreen.CopyToClipboard(this->GetSafeHwnd(), ((CCaptureDlg *)AfxGetMainWnd())->m_rectTracker.m_rect);
	case IDB_CANCEL:
		((CCaptureDlg *)AfxGetMainWnd())->OnCancel();
		break;

	//保存位图到磁盘上
	case IDB_SAVE:
		CFileDialog fileDlg(FALSE);

		fileDlg.m_ofn.lpstrTitle = TEXT("选择位图的保存位置和名称");
		fileDlg.m_ofn.lpstrDefExt = TEXT("bmp");
		fileDlg.m_ofn.lpstrFilter = TEXT("*.bmp(BITMAP Files)\0*.bmp\0\0");

		if (fileDlg.DoModal() == IDOK)
		{
			CDC *pDC = GetDC();
			((CCaptureDlg *)AfxGetMainWnd())->m_bmpScreen.ExportToFile(pDC, fileDlg.GetFileName().GetBuffer(256), ((CCaptureDlg *)AfxGetMainWnd())->m_rectTracker.m_rect);
			ReleaseDC(pDC);

			((CCaptureDlg *)AfxGetMainWnd())->OnCancel();
		}
		break;
	}
}
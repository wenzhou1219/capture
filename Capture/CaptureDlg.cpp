
// CaptureDlg.cpp : 实现文件

#include "stdafx.h"
#include "Capture.h"
#include "CaptureDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


/************************************************************************/
/*用于应用程序“关于”菜单项的 CAboutDlg 对话框								*/
/************************************************************************/
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


/************************************************************************/
/*CCaptureDlg 对话框                                                          */
/************************************************************************/
CCaptureDlg::CCaptureDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCaptureDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_pCoverDlg = NULL;
	m_pToolbarDlg = NULL;

	m_bLeftDown	= FALSE;
}

CCaptureDlg::~CCaptureDlg()
{
	if (NULL != m_pToolbarDlg)
	{
		delete m_pToolbarDlg;
	}
	if (NULL != m_pCoverDlg)
	{
		delete m_pCoverDlg;
	}
}

void CCaptureDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCaptureDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_HOTKEY()
	ON_WM_NCPAINT()
	ON_WM_ERASEBKGND()
	ON_WM_CLOSE()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


/*CCaptureDlg 消息处理程序*/

BOOL CCaptureDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	//获得全屏大小
	m_cxScreen = GetSystemMetrics(SM_CXSCREEN);
	m_cyScreen = GetSystemMetrics(SM_CYSCREEN);

	//设置位图大小
	m_bmpScreen.SetSize(m_cxScreen, m_cyScreen);

	// 设置当前为全屏窗口
	::SetWindowLong(this->m_hWnd, GWL_STYLE, WS_POPUP);
	::SetWindowLong(this->m_hWnd, GWL_EXSTYLE, WS_EX_TOPMOST);//窗口设置为Topmost类型
	SetWindowPos(&CWnd::wndTopMost, 
				 0, 0, 
				 m_cxScreen,
				 m_cyScreen,
				 SWP_HIDEWINDOW);// SetWindowLong设置的值缓存，直到调用SetWindowPos生效

	// 初始化橡皮筋类型
	m_rectTracker.m_rect.SetRect(0, 0, 0, 0);
	m_rectTracker.m_nHandleSize = 5;
	m_rectTracker.m_nStyle = CRectTracker::resizeInside | CRectTracker::solidLine;

	//创建遮盖层窗口
	m_pCoverDlg = new CCoverDlg;
	m_pCoverDlg->Create(MAKEINTRESOURCE(IDD_COVER_DIALOG),
						NULL);

	//创建工具栏对话框
	m_pToolbarDlg = new CToolbarDlg;
	m_pToolbarDlg->Create(MAKEINTRESOURCE(IDD_TOOLBAR_DIALOG),
						  m_pCoverDlg);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CCaptureDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

/************************************************************************/
/* 热键响应                                                                     */
/************************************************************************/
void CCaptureDlg::OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (nHotKeyId = theApp.m_hotkey.GetHotkeyId())
	{
		//防止重复
		if(IsWindowVisible())
		{
			return;
		}

		//停止桌面更新
		GetDesktopWindow()->LockWindowUpdate();

		//获得屏幕截图
		CDC *pDC;

		pDC = GetDesktopWindow()->GetDC();
		m_bmpScreen.GetScreenBmp(pDC);

		ReleaseDC(pDC);

		//允许桌面更新
		::LockWindowUpdate(NULL);

		//显示和隐藏窗口
		SetWindowPos(&CWnd::wndTopMost, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
		m_pCoverDlg->SetWindowPos(this, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
		m_pToolbarDlg->SetWindowPos(m_pCoverDlg, 0, 0, 0, 0, SWP_HIDEWINDOW | SWP_NOSIZE | SWP_NOMOVE);
	}

	CDialog::OnHotKey(nHotKeyId, nKey1, nKey2);
}

void CCaptureDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
HCURSOR CCaptureDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/************************************************************************/
/* 窗口重绘函数                                                                     */
/************************************************************************/

void CCaptureDlg::OnNcPaint()
{
	//程序一启动就隐藏对话框,只运行这一次
	static BOOL bISHideWindow = TRUE;
	if (TRUE == bISHideWindow)
	{
		ShowWindow(SW_HIDE);
		bISHideWindow = FALSE;
	}
}

BOOL CCaptureDlg::OnEraseBkgnd(CDC* pDC)
{
	// 以桌面截图为背景
	m_bmpScreen.BitBltTo(pDC);

	// 绘制橡皮筋区域
	m_rectTracker.Draw(pDC);

	ValidateRect(NULL);

	return TRUE;
}

void CCaptureDlg::ReShowToolbar()
{
	//选择完成则显示工具栏
	m_pToolbarDlg->SetWindowPos(m_pCoverDlg, 
								m_rectTracker.m_rect.left,
								m_rectTracker.m_rect.bottom + 10,
								0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);
	
	//将焦点返回到遮盖层窗口以便进行微调操作
	m_pCoverDlg->SetFocus();
}

/************************************************************************/
/* 隐藏、显示和关闭窗口                                                         */
/************************************************************************/
void CCaptureDlg::OnClose()
{
	// 退出程序
	m_pCoverDlg->DestroyWindow();
	DestroyWindow();

	CDialog::OnClose();
}

void CCaptureDlg::OnOK()
{
	return;
}

void CCaptureDlg::OnCancel()
{
	ShowWindow(SW_HIDE);
	m_pCoverDlg->ShowWindow(SW_HIDE);
	m_pToolbarDlg->ShowWindow(SW_HIDE);
}

/************************************************************************/
/* 各种交互效果绘制主要由鼠标操作完成										*/
/************************************************************************/

BOOL CCaptureDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	// 设置鼠标形状
	if (pWnd == this && m_rectTracker.SetCursor(this, nHitTest))
	{
		return TRUE;
	} 

	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}

void CCaptureDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (FALSE == m_bLeftDown)
	{
		m_bLeftDown = TRUE;

		m_pToolbarDlg->ShowWindow(SW_HIDE);
	}

	// 设置Track
	if(m_rectTracker.HitTest(point)<0)
	{
		m_rectTracker.TrackRubberBand(this,point,TRUE);
		SendMessage(WM_LBUTTONUP);

		m_rectTracker.m_rect.NormalizeRect();
	}
	else
	{
		m_rectTracker.Track(this,point,TRUE);
		SendMessage(WM_LBUTTONUP);

		m_rectTracker.m_rect.NormalizeRect();
	}

	CDialog::OnLButtonDown(nFlags, point);
}

void CCaptureDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (TRUE == m_bLeftDown)
	{
		m_bLeftDown = FALSE;
		Invalidate(TRUE);

		ReShowToolbar();
	}

	CDialog::OnLButtonUp(nFlags, point);
}

void CCaptureDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if (TRUE == m_bLeftDown)
	{
		Invalidate(TRUE);	//所有的绘制工作都集中在WM_PAINT消息中处理
	}

	CDialog::OnMouseMove(nFlags, point);
}

void CCaptureDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	OnCancel();

	CDialog::OnRButtonDown(nFlags, point);
}

void CCaptureDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	//复制选择的区域位图到剪切板上
	m_bmpScreen.CopyToClipboard(this->GetSafeHwnd(), m_rectTracker.m_rect);

	//复制完成后隐藏窗口
	OnCancel();

	CDialog::OnMButtonDblClk(nFlags, point);
}

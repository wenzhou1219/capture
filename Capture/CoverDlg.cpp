// CoverDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Capture.h"
#include "CoverDlg.h"
#include "CaptureDlg.h"

// CCoverDlg 对话框

IMPLEMENT_DYNAMIC(CCoverDlg, CDialog)

CCoverDlg::CCoverDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoverDlg::IDD, pParent)
{
}

CCoverDlg::~CCoverDlg()
{
}

void CCoverDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCoverDlg, CDialog)
	ON_WM_NCPAINT()
	ON_WM_CLOSE()
	ON_WM_CTLCOLOR()
	ON_WM_PAINT()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CCoverDlg 消息处理程序

BOOL CCoverDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//获得全屏大小
	m_cxScreen = GetSystemMetrics(SM_CXSCREEN);
	m_cyScreen = GetSystemMetrics(SM_CYSCREEN);

	// 设置当前为遮盖窗口
	::SetWindowLong(this->m_hWnd, GWL_STYLE, WS_POPUP);
	::SetWindowLong(this->m_hWnd, GWL_EXSTYLE, WS_EX_LAYERED);
	SetWindowPos(theApp.m_pMainWnd, 
				0, 0, 
				GetSystemMetrics(SM_CXSCREEN),
				GetSystemMetrics(SM_CYSCREEN),
				SWP_HIDEWINDOW);
	SetLayeredWindowAttributes(0, 128, LWA_ALPHA);

	//窗口区域
	m_screenRgn.CreateRectRgn(0, 0, m_cxScreen, m_cyScreen);

	return TRUE;
}

void CCoverDlg::OnNcPaint()
{
	//程序一启动就隐藏对话框,只运行这一次
	static BOOL bISHideWindow = TRUE;
	if (TRUE == bISHideWindow)
	{
		ShowWindow(SW_HIDE);
		bISHideWindow = FALSE;
	}
}

HBRUSH CCoverDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// 设置窗口背景颜色
	if (CTLCOLOR_DLG == nCtlColor && pWnd == this)
	{
		return (HBRUSH)GetStockObject(GRAY_BRUSH);
	}

	return hbr;
}

/************************************************************************/
/* 绘制遮罩层                                                                  */
/************************************************************************/
void CCoverDlg::OnPaint()
{
	//设置窗口显示区域
	CRect rectTracker;
	CRgn rgnTracker;
	CRgn rgnScreen;

	rgnTracker.CreateRectRgnIndirect(((CCaptureDlg *)(theApp.m_pMainWnd))->m_rectTracker.m_rect);
	rgnScreen.CreateRectRgn(0, 0, m_cxScreen, m_cyScreen);
	m_wndRgn.CreateRectRgn(0, 0, m_cxScreen, m_cyScreen);
	m_wndRgn.CombineRgn(&rgnScreen, &rgnTracker, RGN_DIFF);

	SetWindowRgn(m_wndRgn, TRUE);

	//这里的Rgn由窗体托管，要注意分离，下次重新设置时会自动删除这里的Rgn
	m_wndRgn.Detach();
}

/************************************************************************/
/* 显示、隐藏和退出全部映射到主窗口上                                          */
/************************************************************************/
void CCoverDlg::OnClose()
{
	((CCaptureDlg *)AfxGetMainWnd())->OnClose();
}

void CCoverDlg::OnOK()
{
	((CCaptureDlg *)AfxGetMainWnd())->OnOK();
}

void CCoverDlg::OnCancel()
{
	((CCaptureDlg *)AfxGetMainWnd())->OnCancel();
}
